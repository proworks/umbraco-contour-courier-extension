﻿//------------------------------------------------------------------------------
// <auto-generated>This code was generated by LLBLGen Pro v3.1.</auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using FluentNHibernate.Mapping;
using Umbraco.Courier.Contour.Provider.EntityClasses;

namespace Umbraco.Courier.Contour.Provider.Mappings
{
	/// <summary>Represents the mapping of the 'Ufform' entity, represented by the 'Ufform' class.</summary>
	public partial class UfformMap : ClassMap<Ufform>
    {
		/// <summary>Initializes a new instance of the <see cref="UfformMap"/> class.</summary>
		public UfformMap()
        {
			Table("[dbo].[UFForms]");
			OptimisticLock.None();
			LazyLoad();

            Id(x => x.Id)
                .Access.CamelCaseField(Prefix.Underscore)
                .Column("[Id]")
                .GeneratedBy.Assigned();

			Map(x=>x.Archived).Column("[Archived]").Not.Nullable().Access.CamelCaseField(Prefix.Underscore);
			Map(x=>x.Created).Column("[Created]").Not.Nullable().Access.CamelCaseField(Prefix.Underscore);
			Map(x=>x.DisableDefaultStylesheet).Column("[DisableDefaultStylesheet]").Not.Nullable().Access.CamelCaseField(Prefix.Underscore);
			Map(x=>x.Entries).Column("[Entries]").Not.Nullable().Access.CamelCaseField(Prefix.Underscore);
			Map(x=>x.FieldIndicationType).Column("[FieldIndicationType]").Access.CamelCaseField(Prefix.Underscore);
			Map(x=>x.GotoPageOnSubmit).Column("[GotoPageOnSubmit]").Not.Nullable().Access.CamelCaseField(Prefix.Underscore);
			Map(x=>x.HideFieldValidation).Column("[HideFieldValidation]").Not.Nullable().Access.CamelCaseField(Prefix.Underscore);
			Map(x=>x.Indicator).Column("[Indicator]").Access.CamelCaseField(Prefix.Underscore);
			Map(x=>x.InvalidErrorMessage).Column("[InvalidErrorMessage]").Access.CamelCaseField(Prefix.Underscore);
			Map(x=>x.ManualApproval).Column("[ManualApproval]").Not.Nullable().Access.CamelCaseField(Prefix.Underscore);
			Map(x=>x.MessageOnSubmit).Column("[MessageOnSubmit]").Access.CamelCaseField(Prefix.Underscore);
			Map(x=>x.Name).Column("[Name]").Not.Nullable().Access.CamelCaseField(Prefix.Underscore);
			Map(x=>x.RequiredErrorMessage).Column("[RequiredErrorMessage]").Access.CamelCaseField(Prefix.Underscore);
			Map(x=>x.ShowValidationSummary).Column("[ShowValidationSummary]").Not.Nullable().Access.CamelCaseField(Prefix.Underscore);
			Map(x=>x.StoreRecordsLocally).Column("[StoreRecordsLocally]").Not.Nullable().Access.CamelCaseField(Prefix.Underscore);
			Map(x=>x.Views).Column("[Views]").Not.Nullable().Access.CamelCaseField(Prefix.Underscore);

			References(x=>x.UfdataSource)
				.Access.CamelCaseField(Prefix.Underscore)
				.Cascade.All()
				.Fetch.Select()
				.Columns("[DataSource]");
			HasMany(x=>x.UfdataSourceMappings)
				.Access.CamelCaseField(Prefix.Underscore)
				.Cascade.AllDeleteOrphan()
				.Fetch.Select()
				.AsSet()
				.Inverse()
				.LazyLoad()
				.KeyColumns.Add("[Form]");
			HasMany(x=>x.Ufpages)
				.Access.CamelCaseField(Prefix.Underscore)
				.Cascade.AllDeleteOrphan()
				.Fetch.Select()
				.AsSet()
				.Inverse()
				.LazyLoad()
				.KeyColumns.Add("[Form]");
			HasMany(x=>x.UfuserFormSecurities)
				.Access.CamelCaseField(Prefix.Underscore)
				.Cascade.AllDeleteOrphan()
				.Fetch.Select()
				.AsSet()
				.Inverse()
				.LazyLoad()
				.KeyColumns.Add("[Form]");
			HasMany(x=>x.Ufworkflows)
				.Access.CamelCaseField(Prefix.Underscore)
				.Cascade.AllDeleteOrphan()
				.Fetch.Select()
				.AsSet()
				.Inverse()
				.LazyLoad()
				.KeyColumns.Add("[Form]");

			AdditionalMappingInfo();
		} 
				
		/// <summary>Partial method for adding additional mapping information in a partial class.</summary>
		partial void AdditionalMappingInfo();
	} 
}  
