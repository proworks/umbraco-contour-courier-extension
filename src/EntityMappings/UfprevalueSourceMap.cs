﻿//------------------------------------------------------------------------------
// <auto-generated>This code was generated by LLBLGen Pro v3.1.</auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using FluentNHibernate.Mapping;
using Umbraco.Courier.Contour.Provider.EntityClasses;

namespace Umbraco.Courier.Contour.Provider.Mappings
{
	/// <summary>Represents the mapping of the 'UfprevalueSource' entity, represented by the 'UfprevalueSource' class.</summary>
	public partial class UfprevalueSourceMap : ClassMap<UfprevalueSource>
    {
		/// <summary>Initializes a new instance of the <see cref="UfprevalueSourceMap"/> class.</summary>
		public UfprevalueSourceMap()
        {
			Table("[dbo].[UFPrevalueSources]");
			OptimisticLock.None();
			LazyLoad();

			Id(x=>x.Id)
				.Access.CamelCaseField(Prefix.Underscore)
				.Column("[Id]")
				.GeneratedBy.Guid();
			Map(x=>x.Name).Column("[Name]").Access.CamelCaseField(Prefix.Underscore);
			Map(x=>x.Type).Column("[Type]").Access.CamelCaseField(Prefix.Underscore);

			HasMany(x=>x.UfprevalueSourceSettings)
				.Access.CamelCaseField(Prefix.Underscore)
				.Cascade.AllDeleteOrphan()
				.Fetch.Select()
				.AsSet()
				.Inverse()
				.LazyLoad()
				.KeyColumns.Add("[PrevalueProvider]");

			AdditionalMappingInfo();
		} 
				
		/// <summary>Partial method for adding additional mapping information in a partial class.</summary>
		partial void AdditionalMappingInfo();
	} 
}  
