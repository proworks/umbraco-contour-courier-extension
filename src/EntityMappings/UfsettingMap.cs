﻿//------------------------------------------------------------------------------
// <auto-generated>This code was generated by LLBLGen Pro v3.1.</auto-generated>
//------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using FluentNHibernate.Mapping;
using Umbraco.Courier.Contour.Provider.EntityClasses;

namespace Umbraco.Courier.Contour.Provider.Mappings
{
	/// <summary>Represents the mapping of the 'Ufsetting' entity, represented by the 'Ufsetting' class.</summary>
	public partial class UfsettingMap : ClassMap<Ufsetting>
    {
		/// <summary>Initializes a new instance of the <see cref="UfsettingMap"/> class.</summary>
		public UfsettingMap()
        {
			Table("[dbo].[UFSettings]");
			OptimisticLock.None();
			LazyLoad();

			CompositeId()
				.KeyReference(x => x.Id, x => x.Access.CamelCaseField(Prefix.Underscore), "[Id]")
				.KeyProperty(x => x.Key, x => x.Access.CamelCaseField(Prefix.Underscore).ColumnName("[Key]"));
			Map(x=>x.Value).Column("[Value]").Access.CamelCaseField(Prefix.Underscore);
			References(x => x.Id)
				.Access.CamelCaseField(Prefix.Underscore)
				.Cascade.All()
				.Fetch.Select()
				.Columns("[Id]")
				.Not.Insert();

			AdditionalMappingInfo();
		} 
				
		/// <summary>Partial method for adding additional mapping information in a partial class.</summary>
		partial void AdditionalMappingInfo();
	} 
}  
