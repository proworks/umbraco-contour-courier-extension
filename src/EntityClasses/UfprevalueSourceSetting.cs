﻿//------------------------------------------------------------------------------
// <auto-generated>This code was generated by LLBLGen Pro v3.1.</auto-generated>
//------------------------------------------------------------------------------
using System;
using System.ComponentModel;
using System.Collections.Generic;

namespace Umbraco.Courier.Contour.Provider.EntityClasses
{
	/// <summary>Class which represents the entity 'UfprevalueSourceSetting'</summary>
	public partial class UfprevalueSourceSetting
	{
		#region Class Member Declarations
		private UfprevalueSource _ufprevalueSource;
		private System.String _key;
		private System.String _value;
		#endregion

		/// <summary>Initializes a new instance of the <see cref="UfprevalueSourceSetting"/> class.</summary>
		public UfprevalueSourceSetting() : base()
		{
			OnCreated();
		}

		/// <summary>Method called from the constructor</summary>
		partial void OnCreated();

		/// <summary>Returns a hash code for this instance.</summary>
		/// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
		public override int GetHashCode()
		{
			int toReturn = base.GetHashCode();
			return toReturn;
		}
	
		/// <summary>Determines whether the specified object is equal to this instance.</summary>
		/// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
		/// <returns><c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.</returns>
		public override bool Equals(object obj)
		{
			return object.ReferenceEquals(this, obj);
		}
		

		#region Class Property Declarations
		/// <summary>Gets or sets the Key field. </summary>	
		public virtual System.String Key
		{ 
			get { return _key; }
			set { _key = value; }
		}

		/// <summary>Gets or sets the Value field. </summary>	
		public virtual System.String Value
		{ 
			get { return _value; }
			set { _value = value; }
		}

		/// <summary>Represents the navigator which is mapped onto the association 'UfprevalueSourceSetting.UfprevalueSource - UfprevalueSource.UfprevalueSourceSettings (m:1)'</summary>
		public virtual UfprevalueSource UfprevalueSource
		{
			get { return _ufprevalueSource; }
			set { _ufprevalueSource = value; }
		}
		
		#endregion
	}
}
