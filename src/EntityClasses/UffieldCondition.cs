﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using Contour.Addons.CourierSupport.ItemProviders.Entities;

namespace Contour.Addons.CourierSupport.EntityClasses
{
	public partial class UffieldCondition
	{
		#region Class Member Declarations
		private Uffield _uffield;
		private Iesi.Collections.Generic.ISet<UffieldConditionRule> _uffieldConditionRules;
		private System.Boolean _enabled;
		private System.Int32 _actionType;
		private System.Int32 _logicType;
		private System.Guid _id;
		#endregion

		/// <summary>Initializes a new instance of the <see cref="Uffield"/> class.</summary>
		public UffieldCondition() : base()
		{
			_uffieldConditionRules = new Iesi.Collections.Generic.HashedSet<UffieldConditionRule>();
			OnCreated();
		}

		/// <summary>Method called from the constructor</summary>
		partial void OnCreated();

		/// <summary>Returns a hash code for this instance.</summary>
		/// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
		public override int GetHashCode()
		{
			int toReturn = base.GetHashCode();
			toReturn ^= this.Id.GetHashCode();
			return toReturn;
		}
	
		/// <summary>Determines whether the specified object is equal to this instance.</summary>
		/// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
		/// <returns><c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.</returns>
		public override bool Equals(object obj)
		{
			if(obj == null) 
			{
				return false;
			}
			var toCompareWith = obj as UffieldCondition;
			return toCompareWith != null && ((this.Id == toCompareWith.Id));
		}
		

		#region Class Property Declarations
		/// <summary>Gets or sets the Enabled field. </summary>	
		public virtual System.Boolean Enabled
		{ 
			get { return _enabled; }
			set { _enabled = value; }
		}

		/// <summary>Gets or sets the ActionType field. </summary>	
		public virtual System.Int32 ActionType
		{ 
			get { return _actionType; }
			set { _actionType = value; }
		}

		/// <summary>Gets or sets the LogicType field. </summary>	
		public virtual System.Int32 LogicType
		{ 
			get { return _logicType; }
			set { _logicType = value; }
		}

		/// <summary>Gets or sets the Id field. </summary>	
		public virtual System.Guid Id
		{ 
			get { return _id; }
			set { _id = value; }
		}

		/// <summary>Represents the navigator which is mapped onto the association 'UffieldCondition.Uffield - Uffield.UffieldConditions (m:1)'</summary>
		public virtual Uffield Uffield
		{
			get { return _uffield; }
			set { _uffield = value; }
		}

		/// <summary>Represents the navigator which is mapped onto the association 'UffieldConditionRule.UffieldCondition - UffieldCondition.UffieldConditionRules (m:1)'</summary>
		public virtual Iesi.Collections.Generic.ISet<UffieldConditionRule> UffieldConditionRules
		{
			get { return _uffieldConditionRules; }
			set { _uffieldConditionRules = value; }
		}
		
		#endregion
	}
}