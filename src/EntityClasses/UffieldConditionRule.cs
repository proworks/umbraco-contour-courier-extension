﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Web;
using Contour.Addons.CourierSupport.ItemProviders.Entities;

namespace Contour.Addons.CourierSupport.EntityClasses
{
	public partial class UffieldConditionRule
	{
		#region Class Member Declarations
		private UffieldCondition _uffieldCondition;
		private Uffield _uffield;
		private System.Guid _id;
		private System.Int32 _operator;
		private System.String _value;
		#endregion

		/// <summary>Initializes a new instance of the <see cref="UffieldConditionRule"/> class.</summary>
		public UffieldConditionRule() : base()
		{
			OnCreated();
		}

		/// <summary>Method called from the constructor</summary>
		partial void OnCreated();

		/// <summary>Returns a hash code for this instance.</summary>
		/// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
		public override int GetHashCode()
		{
			int toReturn = base.GetHashCode();
			toReturn ^= this.Id.GetHashCode();
			return toReturn;
		}
	
		/// <summary>Determines whether the specified object is equal to this instance.</summary>
		/// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
		/// <returns><c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.</returns>
		public override bool Equals(object obj)
		{
			if(obj == null) 
			{
				return false;
			}
			var toCompareWith = obj as UffieldConditionRule;
			return toCompareWith != null && ((this.Id == toCompareWith.Id));
		}
		

		#region Class Property Declarations

		/// <summary>Gets or sets the Id field. </summary>	
		public virtual System.Guid Id
		{ 
			get { return _id; }
			set { _id = value; }
		}

		/// <summary>Gets or sets the Operator field. </summary>	
		public virtual System.Int32 Operator
		{ 
			get { return _operator; }
			set { _operator = value; }
		}

		/// <summary>Gets or sets the Value field. </summary>	
		public virtual System.String Value
		{ 
			get { return _value; }
			set { _value = value; }
		}

		/// <summary>Represents the navigator which is mapped onto the association 'UffieldConditionRule.UffieldCondition - UffieldCondition.UffieldConditionRules (m:1)'</summary>
		public virtual UffieldCondition UffieldCondition
		{
			get { return _uffieldCondition; }
			set { _uffieldCondition = value; }
		}

		/// <summary>Represents the navigator which is mapped onto the association 'UffieldConditionRule.Uffield - Uffield.UffieldConditionRules (m:1)'</summary>
		public virtual Uffield Uffield
		{
			get { return _uffield; }
			set { _uffield = value; }
		}
		
		#endregion
	}
}