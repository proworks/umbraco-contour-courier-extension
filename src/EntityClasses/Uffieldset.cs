﻿//------------------------------------------------------------------------------
// <auto-generated>This code was generated by LLBLGen Pro v3.1.</auto-generated>
//------------------------------------------------------------------------------
using System;
using System.ComponentModel;
using System.Collections.Generic;

namespace Umbraco.Courier.Contour.Provider.EntityClasses
{
	/// <summary>Class which represents the entity 'Uffieldset'</summary>
	public partial class Uffieldset
	{
		#region Class Member Declarations
		private Iesi.Collections.Generic.ISet<Uffield> _uffields;
		private Ufpage _ufpage;
		private System.String _caption;
		private System.Guid _id;
		private System.Int32 _sortOrder;
		#endregion

		/// <summary>Initializes a new instance of the <see cref="Uffieldset"/> class.</summary>
		public Uffieldset() : base()
		{
			_uffields = new Iesi.Collections.Generic.HashedSet<Uffield>();
			OnCreated();
		}

		/// <summary>Method called from the constructor</summary>
		partial void OnCreated();

		/// <summary>Returns a hash code for this instance.</summary>
		/// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
		public override int GetHashCode()
		{
			int toReturn = base.GetHashCode();
			toReturn ^= this.Id.GetHashCode();
			return toReturn;
		}
	
		/// <summary>Determines whether the specified object is equal to this instance.</summary>
		/// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
		/// <returns><c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.</returns>
		public override bool Equals(object obj)
		{
			if(obj == null) 
			{
				return false;
			}
			Uffieldset toCompareWith = obj as Uffieldset;
			return toCompareWith == null ? false : ((this.Id == toCompareWith.Id));
		}
		

		#region Class Property Declarations
		/// <summary>Gets or sets the Caption field. </summary>	
		public virtual System.String Caption
		{ 
			get { return _caption; }
			set { _caption = value; }
		}

		/// <summary>Gets or sets the Id field. </summary>	
		public virtual System.Guid Id
		{ 
			get { return _id; }
			set { _id = value; }
		}

		/// <summary>Gets or sets the SortOrder field. </summary>	
		public virtual System.Int32 SortOrder
		{ 
			get { return _sortOrder; }
			set { _sortOrder = value; }
		}

		/// <summary>Represents the navigator which is mapped onto the association 'Uffield.Uffieldset - Uffieldset.Uffields (m:1)'</summary>
		public virtual Iesi.Collections.Generic.ISet<Uffield> Uffields
		{
			get { return _uffields; }
			set { _uffields = value; }
		}
		
		/// <summary>Represents the navigator which is mapped onto the association 'Uffieldset.Ufpage - Ufpage.Uffieldsets (m:1)'</summary>
		public virtual Ufpage Ufpage
		{
			get { return _ufpage; }
			set { _ufpage = value; }
		}
		
		#endregion
	}
}
