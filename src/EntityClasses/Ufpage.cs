﻿//------------------------------------------------------------------------------
// <auto-generated>This code was generated by LLBLGen Pro v3.1.</auto-generated>
//------------------------------------------------------------------------------
using System;
using System.ComponentModel;
using System.Collections.Generic;

namespace Umbraco.Courier.Contour.Provider.EntityClasses
{
	/// <summary>Class which represents the entity 'Ufpage'</summary>
	public partial class Ufpage
	{
		#region Class Member Declarations
		private Iesi.Collections.Generic.ISet<Uffieldset> _uffieldsets;
		private Ufform _ufform;
		private System.String _caption;
		private System.Guid _id;
		private System.Int32 _sortOrder;
		#endregion

		/// <summary>Initializes a new instance of the <see cref="Ufpage"/> class.</summary>
		public Ufpage() : base()
		{
			_uffieldsets = new Iesi.Collections.Generic.HashedSet<Uffieldset>();
			OnCreated();
		}

		/// <summary>Method called from the constructor</summary>
		partial void OnCreated();

		/// <summary>Returns a hash code for this instance.</summary>
		/// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
		public override int GetHashCode()
		{
			int toReturn = base.GetHashCode();
			toReturn ^= this.Id.GetHashCode();
			return toReturn;
		}
	
		/// <summary>Determines whether the specified object is equal to this instance.</summary>
		/// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
		/// <returns><c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.</returns>
		public override bool Equals(object obj)
		{
			if(obj == null) 
			{
				return false;
			}
			Ufpage toCompareWith = obj as Ufpage;
			return toCompareWith == null ? false : ((this.Id == toCompareWith.Id));
		}
		

		#region Class Property Declarations
		/// <summary>Gets or sets the Caption field. </summary>	
		public virtual System.String Caption
		{ 
			get { return _caption; }
			set { _caption = value; }
		}

		/// <summary>Gets or sets the Id field. </summary>	
		public virtual System.Guid Id
		{ 
			get { return _id; }
			set { _id = value; }
		}

		/// <summary>Gets or sets the SortOrder field. </summary>	
		public virtual System.Int32 SortOrder
		{ 
			get { return _sortOrder; }
			set { _sortOrder = value; }
		}

		/// <summary>Represents the navigator which is mapped onto the association 'Uffieldset.Ufpage - Ufpage.Uffieldsets (m:1)'</summary>
		public virtual Iesi.Collections.Generic.ISet<Uffieldset> Uffieldsets
		{
			get { return _uffieldsets; }
			set { _uffieldsets = value; }
		}
		
		/// <summary>Represents the navigator which is mapped onto the association 'Ufpage.Ufform - Ufform.Ufpages (m:1)'</summary>
		public virtual Ufform Ufform
		{
			get { return _ufform; }
			set { _ufform = value; }
		}
		
		#endregion
	}
}
