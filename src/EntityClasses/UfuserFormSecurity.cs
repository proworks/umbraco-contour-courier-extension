﻿//------------------------------------------------------------------------------
// <auto-generated>This code was generated by LLBLGen Pro v3.1.</auto-generated>
//------------------------------------------------------------------------------
using System;
using System.ComponentModel;
using System.Collections.Generic;

namespace Umbraco.Courier.Contour.Provider.EntityClasses
{
	/// <summary>Class which represents the entity 'UfuserFormSecurity'</summary>
	public partial class UfuserFormSecurity
	{
		#region Class Member Declarations
		private Ufform _ufform;
		private System.Boolean _allowInEditor;
		private System.Boolean _hasAccess;
		private System.Int32 _securityType;
		private System.String _user;
		#endregion

		/// <summary>Initializes a new instance of the <see cref="UfuserFormSecurity"/> class.</summary>
		public UfuserFormSecurity() : base()
		{
			OnCreated();
		}

		/// <summary>Method called from the constructor</summary>
		partial void OnCreated();

		/// <summary>Returns a hash code for this instance.</summary>
		/// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. </returns>
		public override int GetHashCode()
		{
			int toReturn = base.GetHashCode();
			return toReturn;
		}
	
		/// <summary>Determines whether the specified object is equal to this instance.</summary>
		/// <param name="obj">The <see cref="System.Object"/> to compare with this instance.</param>
		/// <returns><c>true</c> if the specified <see cref="System.Object"/> is equal to this instance; otherwise, <c>false</c>.</returns>
		public override bool Equals(object obj)
		{
			return object.ReferenceEquals(this, obj);
		}
		

		#region Class Property Declarations
		/// <summary>Gets or sets the AllowInEditor field. </summary>	
		public virtual System.Boolean AllowInEditor
		{ 
			get { return _allowInEditor; }
			set { _allowInEditor = value; }
		}

		/// <summary>Gets or sets the HasAccess field. </summary>	
		public virtual System.Boolean HasAccess
		{ 
			get { return _hasAccess; }
			set { _hasAccess = value; }
		}

		/// <summary>Gets or sets the SecurityType field. </summary>	
		public virtual System.Int32 SecurityType
		{ 
			get { return _securityType; }
			set { _securityType = value; }
		}

		/// <summary>Gets or sets the User field. </summary>	
		public virtual System.String User
		{ 
			get { return _user; }
			set { _user = value; }
		}

		/// <summary>Represents the navigator which is mapped onto the association 'UfuserFormSecurity.Ufform - Ufform.UfuserFormSecurities (m:1)'</summary>
		public virtual Ufform Ufform
		{
			get { return _ufform; }
			set { _ufform = value; }
		}
		
		#endregion
	}
}
