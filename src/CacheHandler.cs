﻿using System;
using umbraco.cms.businesslogic.macro;
using Umbraco.Core;
using Umbraco.Courier.Core;
using Umbraco.Courier.Core.Interfaces;
using Umbraco.Courier.Core.Helpers;

namespace Umbraco.Courier.Contour.Provider
{
    public class CacheHandler : IApplicationEventHandler
    {
		public void OnApplicationInitialized(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
		{
		}

		public void OnApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
		{
		}

		public void OnApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
		{
			//Source = new Repository(Umbraco.Courier.Core.ProviderModel.RepositoryProviderCollection.Instance.GetProvider(ProviderIDCollection.formItemProviderGuid))
			//{
			//	Provider = { SessionKey = Guid.NewGuid().ToString() }
			//};

			//var isCacheEnabled = Settings.EnableCaching;
		}

		void AfterFormDelete(umbraco.cms.businesslogic.language.Language sender, umbraco.cms.businesslogic.DeleteEventArgs e)
		{
			var itemId = new ItemIdentifier(sender.CultureAlias, ProviderIDCollection.formItemProviderGuid);
			ClearCache(itemId);
		}

		void AfterFormSave(Macro sender, umbraco.cms.businesslogic.SaveEventArgs e)
		{
			var itemId = new ItemIdentifier(sender.Alias, ProviderIDCollection.formItemProviderGuid);
			SendToCache(itemId);
		}

	    private const bool RunAsync = false;
	    private static readonly Repository Source = null;
		private static void SendToCache(ItemIdentifier itemId)
		{
			if (RunAsync)
			{
				var fs = new FileSaver(_sendToCache);
				fs.BeginInvoke(itemId, null, null);
			}
			else
			{
				_sendToCache(itemId);
			}
		}

		private static void ClearCache(ItemIdentifier itemId)
		{
			var provider = Umbraco.Courier.Core.ProviderModel.ItemProviderCollection.Instance.GetProvider(itemId.ProviderId);
			Umbraco.Courier.Core.Cache.ItemCacheManager.Instance.ClearItem(itemId, provider);
		}

		delegate void FileSaver(ItemIdentifier itemId);
		private static void _sendToCache(ItemIdentifier itemId)
		{
			if (!Umbraco.Courier.Core.Cache.ItemCacheManager.Instance.IsCacheable(itemId)) return;
			try
			{
				Source.Provider.SessionKey = Guid.NewGuid().ToString();
				var provider = Umbraco.Courier.Core.ProviderModel.ItemProviderCollection.Instance.GetProvider(itemId.ProviderId);
				Umbraco.Courier.Core.Cache.ItemCacheManager.Instance.ClearItem(itemId, provider);

				//getting the item from the repo
				var it = ((IPackagingTarget)Source.Provider).Package(itemId);

				//store in cache
				Umbraco.Courier.Core.Cache.ItemCacheManager.Instance.StoreItemAsync(it, it.Provider);
			}
			catch (Exception ex)
			{
				Logging._Error(ex.ToString());
			}
		}
    }
}