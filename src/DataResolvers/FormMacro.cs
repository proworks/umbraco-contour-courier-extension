﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Umbraco.Courier.Core.Helpers;
using Umbraco.Courier.Core.ProviderModel;
using Umbraco.Courier.ItemProviders;
using Umbraco.Courier.Core;

namespace Umbraco.Courier.Contour.Provider.DataResolvers
{
    public class FormMacro : ItemDataResolverProvider
    {
        private static readonly List<string> MacroDataTypes = Context.Current.Settings.GetConfigurationCollection("/configuration/itemDataResolvers/macros/add", true);

        public override List<Type> ResolvableTypes
        {
            get { return new List<Type> { typeof(ContentPropertyData), typeof(Template) }; }
        }

        public override bool ShouldExecute(Item item, Umbraco.Courier.Core.Enums.ItemEvent itemEvent)
        {
            if (item.GetType() == typeof(Template) && (itemEvent == Umbraco.Courier.Core.Enums.ItemEvent.Extracting || itemEvent == Umbraco.Courier.Core.Enums.ItemEvent.Packaged))
                return true;
            else if (item.GetType() == typeof(ContentPropertyData))
            {
	            var cpd = (ContentPropertyData)item;
	            return cpd.Data.Any(cp => cp.Value != null && MacroDataTypes.Contains(cp.DataTypeEditor.ToString().ToLower()));
            }
	        return false;
        }

        public override void Packaging(Item item)
        {
			var formProvider = ItemProviderCollection.Instance.GetProvider(ProviderIDCollection.formItemProviderGuid);
			Umbraco.Courier.Core.Cache.ItemCacheManager.Instance.ClearItem(item.ItemId, formProvider);

            if (item.GetType() == typeof(ContentPropertyData))
            {
                var cpd = (ContentPropertyData)item;
                foreach (var prop in cpd.Data.Where(x => x.Value != null && MacroDataTypes.Contains(x.DataTypeEditor.ToString().ToLower())))
                {
                    var content = prop.Value.ToString();

                    AddFormDependencies(content, item);
                }
            }         
        }

		public override void Packaged(Item item)
		{
			if (item.GetType() == typeof(Template) && item.Resources.Any(r => r.PackageFromPath.ToLower().EndsWith(".master")))
			{
				var encoding = Umbraco.Courier.Core.Settings.Encoding;

				var resource = item.Resources.First(r => r.PackageFromPath.ToLower().EndsWith(".master"));
				var contents = resource.ResourceContents;
				var content = encoding.GetString(contents);

				AddFormDependencies(content, item);
			}
		}

        private static void AddFormDependencies(string content, Item item)
        {
            var macroTags = ReferencedMacrosInstring(content);

			foreach (var tag in macroTags.Where(x => x.Alias == "umbracoContour.RazorRenderForm" || x.Alias == "umbracoContour.RenderForm").ToList())
            {
                Guid guid;

                if (tag.Attributes.ContainsKey("formguid") && !string.IsNullOrEmpty(tag.Attributes["formguid"]) && Guid.TryParse(tag.Attributes["formguid"], out guid))
                {
                    var formDep = new Dependency
                    {
	                    ItemId = new ItemIdentifier(guid.ToString(), ProviderIDCollection.formItemProviderGuid),
	                    Name = "Form from form picker"
                    };

	                item.Dependencies.Add(formDep);
                }
            }
        }

		private static IEnumerable<Dependencies.MacroTag> ReferencedMacrosInstring(string str)
		{
			var list = new List<Dependencies.MacroTag>();
			var str1 = Extensions.Replace(Extensions.Replace(str, "<umbraco:Macro", "<umbraco:macro", StringComparison.OrdinalIgnoreCase), "<?UMBRACO_MACRO", "<?umbraco_macro", StringComparison.OrdinalIgnoreCase);
			if (str1.IndexOf("<umbraco:macro", StringComparison.OrdinalIgnoreCase) <= -1 &&
			    str1.IndexOf("<?umbraco_macro", StringComparison.OrdinalIgnoreCase) <= -1) return list;

			var stringBuilder = new StringBuilder(str1);
			var num1 = 0;
			while (true)
			{
				var num2 = stringBuilder.ToString().IndexOf("<?umbraco", StringComparison.OrdinalIgnoreCase);
				if (num2 < 1)
					num2 = stringBuilder.ToString().IndexOf("<?umbraco:macro", StringComparison.OrdinalIgnoreCase);
				if(num2 < 0)
					num2 = stringBuilder.ToString().IndexOf("<?umbraco_macro", StringComparison.OrdinalIgnoreCase);
				if (num2 > -1)
				{
					stringBuilder.Remove(0, num2 + 1);
					var tag = stringBuilder.ToString().Substring(0, stringBuilder.ToString().IndexOf(">", StringComparison.OrdinalIgnoreCase) + 1);
					var dictionary = AttributeList(Dependencies.ReturnAttributes(tag));
					var str2 = string.Empty;
					if (dictionary.ContainsKey("macroalias"))
						str2 = dictionary["macroalias"];
					else if (dictionary.ContainsKey("alias"))
						str2 = dictionary["alias"];
					if (!string.IsNullOrEmpty(str2))
					{
						var macroTag = new Dependencies.MacroTag
						{
							TagLength = tag.Length,
							FoundAtIndex = num2 + num1,
							Alias = str2,
							Attributes = new Dictionary<string, string>()
						};
						foreach (var index in dictionary.Keys)
							macroTag.Attributes.Add(index, dictionary[index]);
						list.Add(macroTag);
					}
					stringBuilder.Remove(0, tag.Length);
					num1 = num2 + tag.Length + num1;
				}
				else
					break;
			}
			return list;
		}

		private static Dictionary<string, string> AttributeList(IDictionary ht)
		{
			var dictionary = new Dictionary<string, string>();
			foreach (var index in ht.Keys.Cast<object>().Where(index => !dictionary.ContainsKey(index.ToString().ToLower())))
			{
				dictionary.Add(index.ToString().ToLower(), ht[index].ToString());
			}
			return dictionary;
		}

	}
}