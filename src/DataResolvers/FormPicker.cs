﻿using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Courier.Core;
using Umbraco.Courier.Core.ProviderModel;
using Umbraco.Courier.ItemProviders;

namespace Umbraco.Courier.Contour.Provider.DataResolvers
{
    public class FormPicker : ItemDataResolverProvider
    {
        private readonly Guid _formPickerGuid = new Guid("60A73E32-C48D-11DE-B452-FC9F55D89593");

        public override List<Type> ResolvableTypes
        {
            get { return new List<Type>() { typeof(ContentPropertyData) }; }
        }

        public override bool ShouldExecute(Item item, Umbraco.Courier.Core.Enums.ItemEvent itemEvent)
        {
            var cpd = (ContentPropertyData)item;
            return (cpd.Data.Any(x => x.DataTypeEditor == _formPickerGuid));
        }

        public override void Packaging(Item item)
        {
			var formProvider = ItemProviderCollection.Instance.GetProvider(ProviderIDCollection.formItemProviderGuid);
			Umbraco.Courier.Core.Cache.ItemCacheManager.Instance.ClearItem(item.ItemId, formProvider);

            var cpd = (ContentPropertyData)item;
            foreach (var cp in cpd.Data)
            {
                Guid guid;

                if (cp.Value != null && cp.DataTypeEditor == _formPickerGuid && Guid.TryParse(cp.Value.ToString(), out guid))
                {
                    var formDep = new Dependency
                    {
	                    ItemId = new ItemIdentifier(guid.ToString(), ProviderIDCollection.formItemProviderGuid),
	                    Name = "Form from form picker"
                    };

	                item.Dependencies.Add(formDep);
                }
            }
        }

        public override void Extracting(Item item)
        {
	        var cpd = (ContentPropertyData)item;
	        foreach (var cp in cpd.Data.Where(cp => cp.Value != null && cp.DataTypeEditor == _formPickerGuid))
	        {
	        }
        }
    }
}