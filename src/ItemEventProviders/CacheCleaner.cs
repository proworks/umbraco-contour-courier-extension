﻿using Umbraco.Courier.Core;
using Umbraco.Courier.Core.ProviderModel;

namespace Umbraco.Courier.Contour.Provider.ItemEventProviders
{
	public class CacheCleaner : ItemEventProvider
	{
		public override string Alias
		{
			get { return "CleanFormCache"; }
		}

		public override void Execute(ItemIdentifier itemId, SerializableDictionary<string, string> parameters)
		{
			var formProvider = ItemProviderCollection.Instance.GetProvider(ProviderIDCollection.formItemProviderGuid);
			Umbraco.Courier.Core.Cache.ItemCacheManager.Instance.ClearItem(itemId, formProvider);
		}
	}
}