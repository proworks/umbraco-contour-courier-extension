﻿using Umbraco.Courier.Contour.Provider.ItemProviders.Entities;
using Umbraco.Courier.Core;
using Umbraco.Courier.Core.ProviderModel;

namespace Umbraco.Courier.Contour.Provider.ItemEventProviders
{
	public class CacheUpdater : ItemEventProvider
	{
		public override string Alias
		{
			get { return "UpdateFormCache"; }
		}

		public override void Execute(ItemIdentifier itemId, SerializableDictionary<string, string> parameters)
		{
			var formProvider = ItemProviderCollection.Instance.GetProvider(ProviderIDCollection.formItemProviderGuid);
			var formItem = formProvider.DatabasePersistence.RetrieveItem<Form>(itemId);
			Umbraco.Courier.Core.Cache.ItemCacheManager.Instance.StoreItem(formItem, formProvider);
		}
	}
}