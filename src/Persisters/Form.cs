﻿using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Courier.Contour.Provider.EntityClasses;
using Umbraco.Courier.Contour.Provider.ItemProviders.Entities;
using NHibernate.Linq;
using Umbraco.Courier.Core;
using Umbraco.Courier.ItemProviders;
using Umbraco.Courier.Persistence.V6.NHibernate;
using Umbraco.Courier.Contour.Provider.Helpers;

// need support for nodeid on submit 
using Umbraco.Forms.Core;
using Umbraco.Forms.Core.Services;
using Field = Umbraco.Courier.Contour.Provider.ItemProviders.Entities.Field;
using FieldSet = Umbraco.Courier.Contour.Provider.ItemProviders.Entities.FieldSet;
using Form = Umbraco.Courier.Contour.Provider.ItemProviders.Entities.Form;
using Page = Umbraco.Courier.Contour.Provider.ItemProviders.Entities.Page;
using PreValue = Umbraco.Courier.Contour.Provider.ItemProviders.Entities.PreValue;

namespace Umbraco.Courier.Contour.Provider.Persisters
{
    [ItemCrud(typeof(Form), typeof(NHibernateProvider))]
    public class FormItem : ItemCrud
    {
        public override List<SystemItem> AvailableItems<T>(ItemIdentifier itemId)
        {
			if(itemId != null)
				return new List<SystemItem>();

			var allItems = new List<SystemItem>();
	        var session = NHibernateProvider.GetCurrentSession();
	        foreach (var c in session.Query<Ufform>())
	        {
		        var si = new SystemItem
		        {
					ItemId = new ItemIdentifier(c.Id.ToString(), ProviderIDCollection.formItemProviderGuid),
					Name = c.Name,
					Description = c.Name + "Form",
					Icon = "icon_form.gif",
					HasChildren = false
		        };
		        allItems.Add(si);
	        }

	        return allItems;
        }

        public override bool DeleteItem<T>(ItemIdentifier itemId)
        {
            throw new NotImplementedException();
        }

        public override T PersistItem<T>(T item)
        {
            var f = item as Form;
            var update = false;

            var pageIds = new List<Guid>();
	        var workflowIds = new List<Guid>();
            var fieldsetIds = new List<Guid>();
            var fieldIds = new List<Guid>();
            var prevalueIds = new List<Guid>();

            var form = FormHelper.Get(f.Id);
            if (form == null)
            {
                form = FormHelper.Create(f);
                NHibernateProvider.GetCurrentSession().Save(form);
            }
            else
            {
                form = FormHelper.Update(form, f);
                NHibernateProvider.GetCurrentSession().Update(form);
                update = true;
            }

            foreach (var p in f.Pages)
            {
                //check if exists
                var page = PageHelper.Get(p.Id);
                pageIds.Add(p.Id);
                if (page == null)
                {
                    page = PageHelper.Create(p, form);
                    NHibernateProvider.GetCurrentSession().Save(page);
                }
                else
                {
                    page = PageHelper.Update(page, p, form);
                    NHibernateProvider.GetCurrentSession().Update(page);
                }

                foreach (var fs in p.FieldSets)
                {
                    fieldsetIds.Add(fs.Id);
                    var fieldset = FieldSetHelper.Get(fs.Id);

                    if (fieldset == null)
                    {
                        fieldset = FieldSetHelper.Create(fs, page);
                        NHibernateProvider.GetCurrentSession().Save(fieldset);
                    }
                    else
                    {
                        fieldset = FieldSetHelper.Update(fieldset,fs, page);
                        NHibernateProvider.GetCurrentSession().Update(fieldset);
                    }

                    foreach (var fld in fs.Fields)
                    {
                        var field = FieldHelper.Get(fld.Id);
                        fieldIds.Add(fld.Id);
                        if (field == null)
                        {
                            field = FieldHelper.Create(fld, fieldset);
                            NHibernateProvider.GetCurrentSession().Save(field);
                        }
                        else
                        {
                            field = FieldHelper.Update(field, fld, fieldset);
                            NHibernateProvider.GetCurrentSession().Update(field);
                        }

                        //only support for default prevalue provider
                        foreach (var pv in fld.PreValues)
                        {
                            var prevalue = PreValueHelper.Get(new Guid(pv.Id.ToString()));
                            prevalueIds.Add(new Guid(pv.Id.ToString()));
                            if (prevalue == null)
                            {
                                prevalue = PreValueHelper.Create(pv, field);
                                NHibernateProvider.GetCurrentSession().Save(prevalue);
                            }
                            else
                            {
                                prevalue = PreValueHelper.Update(prevalue, pv, field);
                                NHibernateProvider.GetCurrentSession().Update(prevalue);
                            }
                        }
                    }
                }
            }

	        foreach (var w in f.Workflows)
	        {
				//check if exists
				var workflow = WorkflowHelper.Get(w.Id);
				workflowIds.Add(w.Id);
				if (workflow == null)
				{
					workflow = WorkflowHelper.Create(w, form);
					NHibernateProvider.GetCurrentSession().Save(workflow);
				}
				else
				{
					workflow = WorkflowHelper.Update(workflow, w, form);
					NHibernateProvider.GetCurrentSession().Update(workflow);
				}
	        }

            
            if (update)
            {
                //deleted items
                var pageIdsToDelete = new List<Guid>();
	            var workflowIdsToDelete = new List<Guid>();
                var fieldsetIdsToDelete = new List<Guid>();
                var fieldIdsToDelete = new List<Guid>();
                var prevalueIdsToDelete = new List<Guid>();
               
                foreach (var p in FormHelper.GetPages(form.Id))
                {
                    if (!pageIds.Contains(p.Id))
                        pageIdsToDelete.Add(p.Id);

                    foreach (var fs in FormHelper.GetFieldSets(p.Id))
                    {
                        if (!fieldsetIds.Contains(fs.Id))
                            fieldsetIdsToDelete.Add(fs.Id);

                        foreach (var fld in FormHelper.GetFields(fs.Id))
                        {
                            if (!fieldIds.Contains(fld.Id))
                                fieldIdsToDelete.Add(fld.Id);

                            foreach (var pv in FormHelper.GetPrevalues(fld.Id))
                            {
                                if (!prevalueIds.Contains(pv.Id))
                                    prevalueIdsToDelete.Add(pv.Id);
                            }
                        }
                    }
                }

				foreach (var w in FormHelper.GetWorkflows(form.Id))
				{
					if (!workflowIds.Contains(w.Id))
						workflowIdsToDelete.Add(w.Id);
				}

                foreach (var pvId in prevalueIdsToDelete)
                {
                    var prevalue = PreValueHelper.Get(pvId);
                    if (prevalue != null)
                    {
                        prevalue.Uffield.Ufprevalues.Remove(prevalue);
                        NHibernateProvider.GetCurrentSession().Delete(prevalue);

                    }
                }

                foreach (var fldId in fieldIdsToDelete)
                {
                    var field = FieldHelper.Get(fldId);
                    if (field != null)
                    {
                        field.Uffieldset.Uffields.Remove(field);
                        NHibernateProvider.GetCurrentSession().Delete(field);
                    }
                }

                foreach (var fsId in fieldsetIdsToDelete)
                {
                    var fieldset = FieldSetHelper.Get(fsId);
                    if (fieldset != null)
                    {
                        fieldset.Ufpage.Uffieldsets.Remove(fieldset);
                        NHibernateProvider.GetCurrentSession().Delete(fieldset);
                    }
                }

                foreach (var pId in pageIdsToDelete)
                {
                    var page = PageHelper.Get(pId);
                    if (page != null)
                    {
                        page.Ufform.Ufpages.Remove(page);
                        NHibernateProvider.GetCurrentSession().Delete(page);
                    }
                }

				foreach (var wId in workflowIdsToDelete)
				{
					var workflow = WorkflowHelper.Get(wId);
					if (workflow != null)
					{
						workflow.Ufform.Ufworkflows.Remove(workflow);
						NHibernateProvider.GetCurrentSession().Delete(workflow);
					}
				}

                //clear cache
                CacheService.ClearCacheItem(f.Id.ToString());
            }
            return f as T;
        }

        public override T RetrieveItem<T>(ItemIdentifier itemId)
        {
            var f = FormHelper.Get(itemId.Id);

            if (f == null)
                return null;

            var item = new Form
            {
	            ItemId = itemId,
	            Id = f.Id,
	            Name = f.Name,
	            Created = f.Created,
				FieldIndicationType = f.FieldIndicationType,
	            Indicator = f.Indicator,
	            ShowValidationSummary = f.ShowValidationSummary,
	            HideFieldValidation = f.HideFieldValidation,
	            RequiredErrorMessage = f.RequiredErrorMessage,
	            InvalidErrorMessage = f.InvalidErrorMessage,
	            MessageOnSubmit = f.MessageOnSubmit,
				GoToPageOnSubmit = PersistenceManager.Default.GetUniqueId(f.GotoPageOnSubmit, NodeObjectTypes.Document),
	            ManualApproval = f.ManualApproval,
	            Archived = f.Archived,
	            StoreRecordsLocally = f.StoreRecordsLocally,
	            DisableDefaultStylesheet = f.DisableDefaultStylesheet
            };

	        //datasource id

            //xpath on submit comes from settings

            foreach (var p in FormHelper.GetPages(f.Id))
            {
                var page = new Page {Caption = p.Caption, Form = f.Id, Id = p.Id, SortOrder = p.SortOrder};

	            foreach (var fs in FormHelper.GetFieldSets(p.Id))
                {
                    var fieldset = new FieldSet
                    {
	                    Id = fs.Id,
	                    Caption = fs.Caption,
	                    Page = p.Id,
	                    SortOrder = fs.SortOrder
                    };

	                foreach (var fld in FormHelper.GetFields(fs.Id))
                    {
                        var field = new Field
                        {
	                        Id = fld.Id,
	                        Caption = fld.Caption,
	                        DataSourceFieldKey = fld.DataSourceField,
	                        FieldSet = fs.Id,
	                        FieldTypeId = fld.Fieldtype,
	                        Form = f.Id,
	                        InvalidErrorMessage = fld.InvalidErrorMessage,
	                        Mandatory = fld.Mandatory,
	                        RegEx = fld.RegEx,
	                        RequiredErrorMessage = fld.RequiredErrorMessage,
	                        SortOrder = fld.SortOrder,
	                        ToolTip = fld.ToolTip
                        };
	                    //field.PreValueSourceId = fld.PreValueProvider

	                    if(fld.PreValueProvider != null)
                            field.PreValueSourceId = (Guid)fld.PreValueProvider;

                        foreach (var pv in FormHelper.GetPrevalues(fld.Id))
                        {
                            var prevalue = new PreValue {Id = pv.Id};
	                        if(pv.SortOrder != null)
                                prevalue.SortOrder = (int)pv.SortOrder;
                            prevalue.Value = pv.Value;
                            prevalue.Field = fld.Id;

                            field.PreValues.Add(prevalue);
                        }

                        fieldset.Fields.Add(field);
                    }

                    page.FieldSets.Add(fieldset);
                }

                item.Pages.Add(page);

	            foreach (var w in FormHelper.GetWorkflows(f.Id))
	            {
					var workflow = new ItemProviders.Entities.Workflow()
		            {
			            Id = w.Id,
						Name = w.Name,
						Form = f.Id,
						Active = w.Active,
						Type = w.Type,
						ExecutesOn = w.ExecutesOn,
						SortOrder = w.SortOrder,
						Settings = new XmlSerializableDictionary<string, string>(w.Ufsettings)
		            };

		            item.Workflows.Add(workflow);
	            }
            }

            return item as T;
        }
    }
}