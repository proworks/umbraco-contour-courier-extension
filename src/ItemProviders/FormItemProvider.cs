﻿using System.Collections.Generic;
using Umbraco.Courier.Core;
using Umbraco.Courier.Contour.Provider.ItemProviders.Entities;

namespace Umbraco.Courier.Contour.Provider.ItemProviders
{
    public class FormItemProvider : ItemProvider
    {
        public FormItemProvider()
        {
            Name = "Forms";
            Description = "Extracts contour forms, accepts a form guid as a valid identifier";
            Id = ProviderIDCollection.formItemProviderGuid;
            ExtractionDirectory = "forms";
            Index = 20;
            ProviderIcon = Umbraco.Core.IO.SystemDirectories.Umbraco + "/images/umbraco/icon_form.gif";
        }

        public override List<SystemItem> AvailableSystemItems()
        {
			return DatabasePersistence.AvailableItems<Form>();
        }

        public override Item HandleDeserialize(ItemIdentifier id, byte[] item)
        {
			Umbraco.Courier.Core.Cache.ItemCacheManager.Instance.ClearItem(id, this);
            return Umbraco.Courier.Core.Serialization.Serializer.Deserialize<Form>(item);
        }

        public override Item HandleExtract(Item item)
        {
			ExecutionContext.ExecuteEvent("UpdateFormCache", item.ItemId, null);

            var f = (Form)item;
			f = DatabasePersistence.PersistItem(f);
			
            return f;
        }

        public override Item HandlePack(ItemIdentifier id)
        {
			Umbraco.Courier.Core.Cache.ItemCacheManager.Instance.ClearItem(id, this);
			var item = DatabasePersistence.RetrieveItem<Form>(id);
            return item;
        }
    }
}