﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Contour.Addons.CourierSupport.ItemProviders.Entities
{
	public class FieldConditionRule
	{
		public FieldConditionRule()
		{
			
		}

		public int Operator { get; set; }
		public string Value { get; set; }
		public int LogicType { get; set; }

		public Guid FieldCondition { get; set; }
		public Guid Field { get; set; }
		public Guid Id { get; set; }
	}
}