﻿using System;
using System.Collections.Generic;

namespace Umbraco.Courier.Contour.Provider.ItemProviders.Entities
{
    public class Field
    {
        public Field()
        {
            PreValues = new List<PreValue>();
            Settings = new XmlSerializableDictionary<string, string>();
        }

        public List<PreValue> PreValues { get; set; }
        public string Caption { get; set; }
        public string ToolTip { get; set; }

        public Object DataSourceFieldKey { get; set; }

        public int SortOrder { get; set; }

        public int PageIndex { get; set; }

        public int FieldsetIndex { get; set; }

        public Guid Id { get; set; }

        public Guid FieldSet { get; set; }

        public Guid Form { get; set; }

        public Guid FieldTypeId { get; set; }

        public bool Mandatory { get; set; }

        public string RegEx { get; set; }

        public string RequiredErrorMessage { get; set; }
        public string InvalidErrorMessage { get; set; }
        public Guid PreValueSourceId { get; set; }
        public XmlSerializableDictionary<string, string> Settings { get; set; }

    }
}