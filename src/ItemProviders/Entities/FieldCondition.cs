﻿using System;
using System.Collections.Generic;

namespace Contour.Addons.CourierSupport.ItemProviders.Entities
{
	public class FieldCondition
	{
		public FieldCondition()
		{
			FieldConditionRules = new List<FieldConditionRule>();
		}

		public List<FieldConditionRule> FieldConditionRules { get; set; }
		public bool Enabled { get; set; }
		public int ActionType { get; set; }
		public int LogicType { get; set; }

		public Guid Field { get; set; }
		public Guid Id { get; set; }
	}
}