﻿using System;
using System.Collections.Generic;

namespace Umbraco.Courier.Contour.Provider.ItemProviders.Entities
{
    public class FieldSet
    {
        public FieldSet()
        {
            Fields = new List<Field>();
        }

        public List<Field> Fields { get; set; }
        public string Caption { get; set; }
        public int SortOrder { get; set; }
        public Guid Id { get; set; }
        public Guid Page { get; set; }

    }
}