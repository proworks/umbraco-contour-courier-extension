﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Umbraco.Courier.Contour.Provider.ItemProviders.Entities
{
	[Serializable]
    [XmlRoot("Dictionary")]
    public class XmlSerializableDictionary<TKey, TValue> : Dictionary<TKey, TValue>, IXmlSerializable
    {
		private IEqualityComparer<TKey> comparer;

        private const string DefaultTagItem = "Item";
        private const string DefaultTagKey = "Key";
        private const string DefaultTagValue = "Value";
        private static readonly XmlSerializer KeySerializer =
                                        new XmlSerializer(typeof(TKey));

        private static readonly XmlSerializer ValueSerializer =
                                        new XmlSerializer(typeof(TValue));

        public XmlSerializableDictionary() : base()
        {
        }

        protected XmlSerializableDictionary(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

		public XmlSerializableDictionary(IDictionary<TKey, TValue> dictionary)
		{
			var stuff = new Dictionary<string, string>();
			if (dictionary == null)
				throw new ArgumentNullException("dictionary");
			foreach (var keyValuePair in dictionary)
				Add(keyValuePair.Key, keyValuePair.Value);
		}





	//	public XmlSerializableDictionary(int capacity)
	//		: this(capacity, (IEqualityComparer<TKey>) null)
	//	{
	//	}

	//	public XmlSerializableDictionary(IEqualityComparer<TKey> comparer)
	//		: this(0, comparer)
	//	{
	//	}


	//public XmlSerializableDictionary(int capacity, IEqualityComparer<TKey> comparer)
	//{
	//  if (capacity < 0)
	//	throw new ArgumentNullException("capacity");
	//  if (capacity > 0)
	//	this.Initialize(capacity);
	//  this.comparer = comparer ?? (IEqualityComparer<TKey>) EqualityComparer<TKey>.Default;
	//}


	//public XmlSerializableDictionary(IDictionary<TKey, TValue> dictionary, IEqualityComparer<TKey> comparer)
	//  : this(dictionary != null ? dictionary.Count : 0, comparer)
	//{
	//  if (dictionary == null)
	//	throw new ArgumentNullException("dictionary");
	//  foreach (var keyValuePair in (IEnumerable<KeyValuePair<TKey, TValue>>) dictionary)
	//	this.Add(keyValuePair.Key, keyValuePair.Value);
	//}



	//private void Initialize(int capacity)
	//{
	//	int prime = HashHelpers.GetPrime(capacity);
	//	this.buckets = new int[prime];
	//	for (int index = 0; index < this.buckets.Length; ++index)
	//		this.buckets[index] = -1;
	//	this.entries = new Dictionary<TKey, TValue>.Entry[prime];
	//	this.freeList = -1;
	//}







        protected virtual string ItemTagName
        {
            get { return DefaultTagItem; }
        }

        protected virtual string KeyTagName
        {
            get { return DefaultTagKey; }
        }

        protected virtual string ValueTagName
        {
            get { return DefaultTagValue; }
        }

        public XmlSchema GetSchema()
        {
            return null;
        }

        public void ReadXml(XmlReader reader)
        {
            bool wasEmpty = reader.IsEmptyElement;

            reader.Read();

            if (wasEmpty)
            {
             return;
            }

            try
            {
                while (reader.NodeType != XmlNodeType.EndElement)
                {
                    reader.ReadStartElement(this.ItemTagName);
                    try
                    {
                        TKey tKey;
                        TValue tValue;

                        reader.ReadStartElement(this.KeyTagName);
                        try
                        {
                            tKey = (TKey)KeySerializer.Deserialize(reader);
                        }
                        finally
                        {
                            reader.ReadEndElement();
                        }

                        reader.ReadStartElement(this.ValueTagName);
                        try
                        {
                            tValue = (TValue)ValueSerializer.Deserialize(reader);
                        }
                        finally
                        {
                            reader.ReadEndElement();
                        }

                        this.Add(tKey, tValue);
                    }
                    finally
                    {
                        reader.ReadEndElement();
                    }

                    reader.MoveToContent();
                }
            }
            finally
            {
                reader.ReadEndElement();
            }
        }

        public void WriteXml(XmlWriter writer)
        {
            foreach (KeyValuePair<TKey, TValue> keyValuePair in this)
            {
                writer.WriteStartElement(this.ItemTagName);
                try
                {
                    writer.WriteStartElement(this.KeyTagName);
                    try
                    {
                        KeySerializer.Serialize(writer, keyValuePair.Key);
                    }
                    finally
                    {
                        writer.WriteEndElement();
                    }

                    writer.WriteStartElement(this.ValueTagName);
                    try
                    {
                        ValueSerializer.Serialize(writer, keyValuePair.Value);
                    }
                    finally
                    {
                        writer.WriteEndElement();
                    }
                }
                finally
                {
                    writer.WriteEndElement();
                }
            }
        }
    }
}