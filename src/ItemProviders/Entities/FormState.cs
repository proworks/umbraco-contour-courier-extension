﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Umbraco.Courier.Contour.Provider.ItemProviders.Entities
{
	public class FormState
	{
		public FormState()
		{
			WorkflowExecutionStates = new List<WorkflowExecutionState>();
		}

		public List<WorkflowExecutionState> WorkflowExecutionStates { get; set; }
		public string State { get; set; }
	}
}