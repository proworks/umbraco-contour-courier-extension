﻿using System;
using System.Collections.Generic;

namespace Umbraco.Courier.Contour.Provider.ItemProviders.Entities
{
    public class Page
    {
        public Page()
        {
            FieldSets = new List<FieldSet>();
        }

        public List<FieldSet> FieldSets { get; set; }
        public string Caption { get; set; }
        public int SortOrder { get; set; }
        public Guid Id { get; set; }
        public Guid Form { get; set; }
    }
}