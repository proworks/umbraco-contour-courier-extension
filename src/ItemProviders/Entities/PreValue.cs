﻿using System;

namespace Umbraco.Courier.Contour.Provider.ItemProviders.Entities
{
    public class PreValue
    {
        public PreValue()
        {

        }

        public Object Id { get; set; }
        public Guid Field { get; set; }
        public string Value { get; set; }
        public int SortOrder { get; set; }
    }
}