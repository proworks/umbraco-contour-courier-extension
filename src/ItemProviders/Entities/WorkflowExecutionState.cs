﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Umbraco.Courier.Contour.Provider.ItemProviders.Entities
{
	public class WorkflowExecutionState
	{
		public Guid FormState { get; set; }
		public Guid Form { get; set; }
		public Guid Workflow { get; set; }
	}
}