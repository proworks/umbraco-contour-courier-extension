﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Umbraco.Courier.Contour.Provider.ItemProviders.Entities
{
	public class Workflow
	{
		public Workflow()
		{
			Settings = new XmlSerializableDictionary<string, string>();
		}

		public Guid Id { get; set; }
		public Guid Type { get; set; }
		public string Name { get; set; }
		public Guid Form { get; set; }
		public bool Active { get; set; }
		public int? ExecutesOn { get; set; }
		public int SortOrder { get; set; }
		public XmlSerializableDictionary<string, string> Settings { get; set; }
	}
}