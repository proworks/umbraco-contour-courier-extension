﻿using System;
using System.Collections.Generic;
using Umbraco.Courier.Core;

namespace Umbraco.Courier.Contour.Provider.ItemProviders.Entities
{
    public class Form: Item
    {
        public Form()
        {
            Pages = new List<Page>();
			Workflows = new List<Workflow>();
        }

        public string Name { get; set; }
        public DateTime Created { get; set; }
        public int? FieldIndicationType { get; set; }
        public string Indicator { get; set; }

        public bool ShowValidationSummary { get; set; }
        public bool HideFieldValidation { get; set; }

        public string RequiredErrorMessage { get; set; }
        public string InvalidErrorMessage { get; set; }

        public string MessageOnSubmit { get; set; }
        public Guid GoToPageOnSubmit { get; set; }
        public string XPathOnSubmit { get; set; }

        public Boolean ManualApproval { get; set; }
        public Boolean Archived { get; set; }
        public Boolean StoreRecordsLocally { get; set; }
        public Boolean DisableDefaultStylesheet { get; set; }
        public List<Page> Pages { get; set; }
		public List<Workflow> Workflows { get; set; } 

        public Guid DataSource { get; set; }
        public Guid Id { get; set; }
    }
}