﻿using System;
using Umbraco.Courier.UI.CourierTasks;

namespace Umbraco.Courier.Contour.Provider.UITasks
{
    public class FormTasks: ItemTaskProvider
    {
        public FormTasks()
        {
            Name = "Contour form tasks";
            Description = "Provides UI Tasks for the Umbraco backend";
            Id = new Guid("D2C26C8E-82CC-40D2-974F-1E65282A47E3");

            ItemProviderId = ProviderIDCollection.formItemProviderGuid;

            TreeAlias = "forms";
            TreeNodeTypeAlias = "forms";
        }

        public override string GetItemIdentifier(object treeNodeId)
        {
            return treeNodeId.ToString();
            
        }
    }
}