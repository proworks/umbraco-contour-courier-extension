﻿using System;
using System.Linq;
using Umbraco.Courier.Contour.Provider.ItemProviders.Entities;
using Umbraco.Courier.Persistence.V6.NHibernate;
using NHibernate.Criterion;

namespace Umbraco.Courier.Contour.Provider.Helpers
{
    public class FieldHelper
    {
        public static EntityClasses.Uffield Create(Field field, EntityClasses.Uffieldset fieldset)
        {
            var f = new EntityClasses.Uffield();
            f = Update(f, field, fieldset);
            return f;
        }

        public static EntityClasses.Uffield Get(string guid)
        {
            return Get(new Guid(guid));
        }
        public static EntityClasses.Uffield Get(Guid guid)
        {
            var field = NHibernateProvider.GetCurrentSession().CreateCriteria<EntityClasses.Uffield>()
                       .Add(Restrictions.Eq("id", guid))
                       .List<EntityClasses.Uffield>().FirstOrDefault();

            return field;
        }

        public static EntityClasses.Uffield Update(EntityClasses.Uffield f, Field field, EntityClasses.Uffieldset fieldset)
        {
            f.Id = field.Id;
            f.Caption = field.Caption;
            f.SortOrder = field.SortOrder;

            f.DataSourceField = field.DataSourceFieldKey.ToString();
            f.Fieldtype = field.FieldTypeId;
         
            f.InvalidErrorMessage = field.InvalidErrorMessage;
            f.Mandatory = field.Mandatory;
            f.RegEx = field.RegEx;
            f.RequiredErrorMessage = field.RequiredErrorMessage;
           
            f.ToolTip = field.ToolTip;
           
           
            f.Uffieldset = fieldset;

            f.PreValueProvider = field.PreValueSourceId;

            return f;
        }
    }
}