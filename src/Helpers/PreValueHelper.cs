﻿using System;
using System.Linq;
using Umbraco.Courier.Contour.Provider.ItemProviders.Entities;
using Umbraco.Courier.Persistence.V6.NHibernate;
using NHibernate.Criterion;

namespace Umbraco.Courier.Contour.Provider.Helpers
{
    public class PreValueHelper
    {
        public static EntityClasses.Ufprevalue Create(PreValue prevalue, EntityClasses.Uffield field)
        {
            var pv = new EntityClasses.Ufprevalue();
            pv = Update(pv, prevalue, field);
            return pv;
        }

        public static EntityClasses.Ufprevalue Get(string guid)
        {
            return Get(new Guid(guid));
        }
        public static EntityClasses.Ufprevalue Get(Guid guid)
        {
            var prevalue = NHibernateProvider.GetCurrentSession().CreateCriteria<EntityClasses.Ufprevalue>()
                       .Add(Restrictions.Eq("id", guid))
                       .List <EntityClasses.Ufprevalue>().FirstOrDefault();

            return prevalue;
        }

        public static EntityClasses.Ufprevalue Update(EntityClasses.Ufprevalue pv, PreValue prevalue, EntityClasses.Uffield field)
        {
            pv.Uffield = field;
            pv.Id = new Guid(prevalue.Id.ToString());
            pv.SortOrder = prevalue.SortOrder;
            pv.Value = prevalue.Value;
           

            return pv;
        }
    }
}