﻿using System;
using System.Linq;
using Contour.Addons.CourierSupport.ItemProviders.Entities;
using Umbraco.Courier.Persistence.V6.NHibernate;
using NHibernate.Criterion;

namespace Contour.Addons.CourierSupport.Helpers
{
    public class FieldConditionRuleHelper
    {
        public static EntityClasses.UffieldConditionRule Create(FieldConditionRule fieldConditionRule, EntityClasses.UffieldCondition fieldCondition)
        {
            var fcr = new EntityClasses.UffieldConditionRule();
            fcr = Update(fcr, fieldConditionRule, fieldCondition);
            return fcr;
        }

        public static EntityClasses.UffieldConditionRule Get(string guid)
        {
            return Get(new Guid(guid));
        }
        public static EntityClasses.UffieldConditionRule Get(Guid guid)
        {
            var fieldConditionRule = NHibernateProvider.GetCurrentSession().CreateCriteria<EntityClasses.UffieldConditionRule>()
                       .Add(Restrictions.Eq("id", guid))
                       .List <EntityClasses.UffieldConditionRule>().FirstOrDefault();

			return fieldConditionRule;
        }

        public static EntityClasses.UffieldConditionRule Update(EntityClasses.UffieldConditionRule fcr, FieldConditionRule fieldConditionRule, EntityClasses.UffieldCondition fieldCondition)
        {
            fcr.UffieldCondition = fieldCondition;
	        fcr.Uffield = fieldCondition.Uffield;
			fcr.Id = new Guid(fieldConditionRule.Id.ToString());
			fcr.Operator = fieldConditionRule.Operator;
			fcr.Value = fieldConditionRule.Value;
           
            return fcr;
        }
    }
}