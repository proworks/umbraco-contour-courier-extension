﻿using System;
using System.Collections.Generic;
using System.Linq;
using Contour.Addons.CourierSupport.EntityClasses;
using Contour.Addons.CourierSupport.ItemProviders.Entities;
using Umbraco.Courier.Persistence.V6.NHibernate;
using NHibernate.Criterion;

namespace Contour.Addons.CourierSupport.Helpers
{
    public class FieldConditionHelper
    {
        public static EntityClasses.UffieldCondition Create(FieldCondition fieldCondition, EntityClasses.Uffield field)
        {
            var fc = new EntityClasses.UffieldCondition();
            fc = Update(fc, fieldCondition, field);
            return fc;
        }

        public static EntityClasses.UffieldCondition Get(string guid)
        {
            return Get(new Guid(guid));
        }
        public static EntityClasses.UffieldCondition Get(Guid guid)
        {
            var fieldCondition = NHibernateProvider.GetCurrentSession().CreateCriteria<EntityClasses.UffieldCondition>()
                       .Add(Restrictions.Eq("id", guid))
                       .List<EntityClasses.UffieldCondition>().FirstOrDefault();

			return fieldCondition;
        }

        public static EntityClasses.UffieldCondition Update(EntityClasses.UffieldCondition fc, FieldCondition fieldCondition, EntityClasses.Uffield field)
        {
			fc.Id = fieldCondition.Id;
			fc.Enabled = fieldCondition.Enabled;
			fc.ActionType = fieldCondition.ActionType;
			fc.LogicType = fieldCondition.LogicType;
			
            fc.Uffield = field;

            return fc;
        }

		public static List<UffieldConditionRule> GetFieldConditionRules(Guid fieldConditionGuid)
		{
			return NHibernateProvider.GetCurrentSession().CreateCriteria<UffieldCondition>()
					   .Add(Restrictions.Eq("id", fieldConditionGuid))
					   .List<UffieldCondition>().FirstOrDefault().UffieldConditionRules.ToList();
		}
    }
}