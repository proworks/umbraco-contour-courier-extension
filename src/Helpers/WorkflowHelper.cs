﻿using System;
using System.Linq;
using Umbraco.Courier.Contour.Provider.EntityClasses;
using NHibernate.Criterion;
using Umbraco.Courier.Persistence.V6.NHibernate;
using Workflow = Umbraco.Courier.Contour.Provider.ItemProviders.Entities.Workflow;

namespace Umbraco.Courier.Contour.Provider.Helpers
{
	public class WorkflowHelper
	{
		public static Ufworkflow Create(Workflow workflow, Ufform form)
		{
			var w = new Ufworkflow();
			w = Update(w, workflow, form);
			return w;
		}

		public static Ufworkflow Get(string guid)
		{
			return Get(new Guid(guid));
		}

		public static Ufworkflow Get(Guid guid)
		{
			var workflow = NHibernateProvider.GetCurrentSession().CreateCriteria<Ufworkflow>()
					   .Add(Restrictions.Eq("id", guid))
					   .List<Ufworkflow>().FirstOrDefault();

			return workflow;
		}

		public static Ufworkflow Update(Ufworkflow w, Workflow workflow, Ufform form)
		{
			w.Id = workflow.Id;
			w.Type = workflow.Type;
			w.SortOrder = workflow.SortOrder;
			w.Ufform = form;
			w.Name = workflow.Name;
			w.Active = workflow.Active;
			w.ExecutesOn = workflow.ExecutesOn;

			// remove unused settings
			foreach (var setting in w.Ufsettings.ToList())
			{
				if (!workflow.Settings.ContainsKey(setting.Key))
				{
					w.Ufsettings.Remove(setting);
				}
			}

			// add new settings
			foreach (var setting in workflow.Settings.ToList())
			{
				if (!w.Ufsettings.ContainsKey(setting.Key))
				{
					w.Ufsettings.Add(setting);
				}
				else
				{
					w.Ufsettings[setting.Key] = setting.Value;
				}
			}
			

			return w;
		}
	}
}