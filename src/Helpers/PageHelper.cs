﻿using System;
using System.Linq;
using Umbraco.Courier.Contour.Provider.ItemProviders.Entities;
using Umbraco.Courier.Persistence.V6.NHibernate;
using NHibernate.Criterion;

namespace Umbraco.Courier.Contour.Provider.Helpers
{
    public class PageHelper
    {
        public static EntityClasses.Ufpage Create(Page page, EntityClasses.Ufform form)
        {
            var p = new EntityClasses.Ufpage();
            p = Update(p, page, form);
            return p;
        }

        public static EntityClasses.Ufpage Get(string guid)
        {
            return Get(new Guid(guid));
        }
        public static EntityClasses.Ufpage Get(Guid guid)
        {
            var page = NHibernateProvider.GetCurrentSession().CreateCriteria<EntityClasses.Ufpage>()
                       .Add(Restrictions.Eq("id", guid))
                       .List<EntityClasses.Ufpage>().FirstOrDefault();

            return page;
        }

        public static EntityClasses.Ufpage Update(EntityClasses.Ufpage p, Page page, EntityClasses.Ufform form)
        {
            p.Id = page.Id;
            p.Caption = page.Caption;
            p.SortOrder = page.SortOrder;
            p.Ufform = form;
           
            return p;
        }
    }
}