﻿using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Courier.Contour.Provider.EntityClasses;
using Umbraco.Courier.Contour.Provider.ItemProviders.Entities;
using Umbraco.Courier.Core;
using Umbraco.Courier.ItemProviders;
using Umbraco.Courier.Persistence.V6.NHibernate;
using NHibernate.Criterion;

namespace Umbraco.Courier.Contour.Provider.Helpers
{
    public class FormHelper
    {
        public static Ufform Create(Form form)
        {
            var f = new Ufform();
            f = Update(f, form);
            return f;
        }

        public static Ufform Get(string guid)
        {
            return Get(new Guid(guid));
        }
        public static Ufform Get(Guid guid)
        {
            var form = NHibernateProvider.GetCurrentSession().CreateCriteria<Ufform>()
                       .Add(Restrictions.Eq("id", guid))
                       .List<Ufform>().FirstOrDefault();

            return form;
        }

        public static Ufform Update(Ufform f, Form form)
        {
            f.Id = form.Id;
            f.Name = form.Name;
            f.Created = form.Created;
			f.FieldIndicationType = form.FieldIndicationType;
            f.Indicator = form.Indicator;
            f.ShowValidationSummary = form.ShowValidationSummary;
            f.HideFieldValidation = form.HideFieldValidation;
            f.RequiredErrorMessage = form.RequiredErrorMessage;
            f.InvalidErrorMessage = form.InvalidErrorMessage;
            f.MessageOnSubmit = form.MessageOnSubmit;
			f.GotoPageOnSubmit = PersistenceManager.Default.GetNodeId(form.GoToPageOnSubmit, NodeObjectTypes.Document);
            
            f.ManualApproval = form.ManualApproval;
            f.Archived = form.Archived;
            f.StoreRecordsLocally = form.StoreRecordsLocally;
            f.DisableDefaultStylesheet = form.DisableDefaultStylesheet;

            //datasource id

            //xpath on submit comes from settings

            return f;
        }

        public static List<Ufpage> GetPages(Guid formGuid)
        {
            return Get(formGuid).Ufpages.ToList();
        }

        public static List<Uffieldset> GetFieldSets(Guid pageGuid)
        {
            return NHibernateProvider.GetCurrentSession().CreateCriteria<Ufpage>()
                       .Add(Restrictions.Eq("id", pageGuid))
                       .List<Ufpage>().FirstOrDefault().Uffieldsets.ToList();
        }

        public static List<Uffield> GetFields(Guid fieldsetGuid)
        {
            return NHibernateProvider.GetCurrentSession().CreateCriteria<Uffieldset>()
                      .Add(Restrictions.Eq("id", fieldsetGuid))
                      .List<Uffieldset>().FirstOrDefault().Uffields.ToList();
        }

        public static List<Ufprevalue> GetPrevalues(Guid fieldGuid)
        {
            return NHibernateProvider.GetCurrentSession().CreateCriteria<Uffield>()
                       .Add(Restrictions.Eq("id", fieldGuid))
                       .List<Uffield>().FirstOrDefault().Ufprevalues.ToList();
        }

	    public static List<Ufworkflow> GetWorkflows(Guid formGuid)
	    {
		    return Get(formGuid).Ufworkflows.ToList();
	    } 
    }
}