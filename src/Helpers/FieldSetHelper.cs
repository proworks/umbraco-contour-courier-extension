﻿using System;
using System.Linq;
using Umbraco.Courier.Contour.Provider.ItemProviders.Entities;
using Umbraco.Courier.Persistence.V6.NHibernate;
using NHibernate.Criterion;

namespace Umbraco.Courier.Contour.Provider.Helpers
{
    public class FieldSetHelper
    {
        public static EntityClasses.Uffieldset Create(FieldSet fieldset, EntityClasses.Ufpage page)
        {
            var fs = new EntityClasses.Uffieldset();
            fs = Update(fs, fieldset, page);
            return fs;
        }

        public static EntityClasses.Uffieldset Get(string guid)
        {
            return Get(new Guid(guid));
        }
        public static EntityClasses.Uffieldset Get(Guid guid)
        {
            var fieldset = NHibernateProvider.GetCurrentSession().CreateCriteria<EntityClasses.Uffieldset>()
                       .Add(Restrictions.Eq("id", guid))
                       .List<EntityClasses.Uffieldset>().FirstOrDefault();

            return fieldset;
        }

        public static EntityClasses.Uffieldset Update(EntityClasses.Uffieldset fs, FieldSet fieldset, EntityClasses.Ufpage page)
        {
            fs.Id = fieldset.Id;
            fs.Caption = fieldset.Caption;
            fs.SortOrder = fieldset.SortOrder;
            fs.Ufpage = page;

            return fs;
        }
    }
}